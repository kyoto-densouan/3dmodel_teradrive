# README #

1/3スケールのSEGA TeraDrive風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK Fusion360です。

***

# 実機情報

## メーカ
- SEGA

## 発売時期
- 1991年5月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%86%E3%83%A9%E3%83%89%E3%83%A9%E3%82%A4%E3%83%96)
- [セガハード大百科](https://sega.jp/history/hard/megadrive/devices.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_teradrive/raw/f39ffacc8ab8e856d71565a30280254fbf88b911/ModelView_model3.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_teradrive/raw/f39ffacc8ab8e856d71565a30280254fbf88b911/ModelView_open_model3.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_teradrive/raw/f39ffacc8ab8e856d71565a30280254fbf88b911/ModelView_model2.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_teradrive/raw/f39ffacc8ab8e856d71565a30280254fbf88b911/ModelView_open_model2.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_teradrive/raw/f39ffacc8ab8e856d71565a30280254fbf88b911/ExampleImage.jpg)
